require "selenium-webdriver"

driver = Selenium::WebDriver.for :firefox
driver.navigate.to "http://google.com"
driver.manage.window.maximize

element = driver.find_element(:id, 'lst-ib')
element.send_keys "Test Script in Ruby!"
element.submit
driver.manage.timeouts.implicit_wait = 100

puts driver.title

driver.quit