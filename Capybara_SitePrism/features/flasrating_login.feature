Feature: Logging into FlashRatings

Background: I am on Flash Ratings login

Scenario Outline:
Given when i submit form with "<email>" and "<password>"
Then I receive a "<message>"

Examples:
| email             | password | message                         |
| ishaan@vinsol.com | 123456   | You have successfully logged in |
