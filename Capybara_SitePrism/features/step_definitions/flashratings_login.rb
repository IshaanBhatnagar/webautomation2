Given(/^when i submit form with "([^"]*)" and "([^"]*)"$/) do |email, password|
  @login_page = LoginPage.new
  @login_page.load
  @login_page.login_with(email, password)
end

Then(/^I receive a "([^"]*)"$/) do |message|
  @login_page.success_message(message)
end
