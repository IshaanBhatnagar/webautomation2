class LoginPage < SitePrism::Page
  set_url 'https://www.flashratings.com/login'

  element :email, "#email"
  element :password, '#password'
  element :login_button, 'body > section > div.registration > div > div > div > div > div > form > input'
  element :message, 'body > section > div.alert.alert-info.flash-message.fade.in'

  def login_with(email, password)
    self.email.set email
    self.password.set password
    login_button.click
  end

  def success_message (message)
    self.message.text.include? message
  end
end