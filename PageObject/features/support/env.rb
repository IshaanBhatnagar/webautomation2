require 'rubygems'
require "selenium-webdriver"
require "rspec"
require "page-object"
require "require_all"
include RSpec::Expectations

require_relative '../../lib/Pages/login_page'

Before do | scenario |

 @browser = Selenium::WebDriver.for :firefox
 @accept_next_alert = true
 @browser.manage.timeouts.implicit_wait = 30
 @wait = Selenium::WebDriver::Wait.new(:timeout => 30)
 @browser.manage.window.maximize

end

After do
 @browser.quit
end