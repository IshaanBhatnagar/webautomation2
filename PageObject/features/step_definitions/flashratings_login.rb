Given(/^I am on Flashratings login page\.$/) do
  @browser.get('https://www.flashratings.com/login')
end

When(/^submit form with "([^"]*)" and "([^"]*)"$/) do |email, password|
  logging_in = LoginPage.new(@browser)
  logging_in.login_with(email, password)
end

Then(/^I receive a "([^"]*)"$/) do |message|
  expect(@browser.find_element(:css, 'body > section > div.alert.alert-info.flash-message.fade.in').text).to include(message)
end
