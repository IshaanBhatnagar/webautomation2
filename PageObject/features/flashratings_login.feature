Feature: Login into Flash Ratings

Background:
Given I am on Flashratings login page.

Scenario Outline:
And submit form with "<email>" and "<password>"
Then I receive a "<message>"

Examples:
| email             | password | message |
| ishaan@vinsol.com | 123456   | success |
