class LoginPage
  include PageObject

  text_field(:email, :id => 'email')
  text_field(:password, :id => 'password')
  button(:login, :value => 'Log in »')

  def login_with(email, password)
    self.email = email
    sleep  2
    self.password = password
    sleep 2
    login
  end
end
