Given(/^I am on a FlashRatings homepage\.$/) do
  @browser.navigate.to "https://www.flashratings.com/"
end

Given(/^I click on Login button\.$/) do
  @browser.find_element(:css, '#container > header > div > div.rightlinks.pull-right.mart0 > a:nth-child(5) > b').click
end

Given(/^I login with "([^"]*)" and "([^"]*)"$/) do |email, password|
  @browser.find_element(:id, 'email').send_keys(email)
  @browser.find_element(:id, 'password').send_keys(password)
  @browser.find_element(:css, 'body > section > div.registration > div > div > div > div > div > form > input').click
end

Then(/^I should see success "([^"]*)"$/) do |message|
  expect(@browser.find_element(:css, 'body > section > div.alert.alert-info.flash-message.fade.in').text).to include(message)
end
