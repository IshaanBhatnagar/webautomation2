Before do | scenario |

 @browser = Selenium::WebDriver.for :firefox
 @accept_next_alert = true
 @browser.manage.timeouts.implicit_wait = 30
 @wait = Selenium::WebDriver::Wait.new(:timeout => 30)
 @browser.manage.window.maximize

end

After do
 @browser.quit
end