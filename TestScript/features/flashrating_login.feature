Feature: Invalid Login
  As a registered User, I should be able to login into 'FlashRatings' site.

Background:
  Given I am on a FlashRatings homepage.
  And I click on Login button.


Scenario Outline: Register with valid credentials
  And I login with "<email>" and "<password>"
  Then I should see success "<message>"

Examples:
  | email             | password | message                          |
  | ishaan@vinsol.com | 123456   | You have successfully logged in. |
