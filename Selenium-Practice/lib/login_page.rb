class LoginPage < SitePrism::Page
  set_url 'http://automationpractice.com/index.php'

  element :sign_in_button, '#header > div.nav > div > div > nav > div.header_user_info > a'
  element :email, '#email'
  element :password, '#passwd'
  element :submit_button, '#SubmitLogin'
  element :warning, '#center_column > div.alert.alert-danger > ol > li'
  # element :password_is_required, '#center_column > div.alert.alert-danger > ol > li'
  # element :authentication_fail, '#center_column > div.alert.alert-danger > ol > li'
  element :welcome, '#center_column > p'
  element :sign_out_button, '#header > div.nav > div > div > nav > div:nth-child(2) > a'

  def click_on_signin
    sign_in_button.click
  end

  def submit_details(email, password)
    self.email.set(email)
    self.password.set(password)
    submit_button.click
  end

  def success_message(message)
    self.welcome
  end

  def fail_message(message)
    self.warning
  end

  def click_on_signout
    sign_out_button.click
    page.has_content?('Authentication')
  end
end