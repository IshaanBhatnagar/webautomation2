require 'capybara'
require 'capybara/rspec'
require 'selenium-webdriver'
require 'site_prism'
require 'byebug'
require 'rspec'
require 'rubygems'
include RSpec::Expectations
require_relative '../../lib/login_page.rb'


Capybara.default_driver = :selenium
