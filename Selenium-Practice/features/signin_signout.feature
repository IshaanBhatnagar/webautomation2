Feature: Sign In and Sign Out
To test sign in and sign out feature in an ecommerce site.

Background:
Given I am at automationpractice.com

Scenario Outline: SignIn Success
When I click on Sign In
And submit "<email>" and "<password>"
Then I receive success "<message>"
And I click on Sign Out and return to Authentication page.

Examples:
| email       | password | message                 |
| abc@xyz.com | Test@123 | Welcome to your account |


Scenario Outline: SignIn Success
When I click on Sign In
And submit "<email>" and "<password>"
Then I receive fail "<message>"

Examples:
| email        | password | message                   |
| abc@xyz.com  | Test@12  | Authentication failed     |
| abc@xyzd.com | Test@123 | Authentication failed     |
|              | Test@123 | An email address required |
| abc@xyz.com  |          | Password is not required  |
