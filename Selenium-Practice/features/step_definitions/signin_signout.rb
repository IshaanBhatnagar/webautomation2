Given(/^I am at automationpractice\.com$/) do
  @login_page = LoginPage.new
  @login_page.load
end

When(/^I click on Sign In$/) do
  @login_page.click_on_signin
end

When(/^submit "([^"]*)" and "([^"]*)"$/) do |email, password|
  @login_page.submit_details(email, password)
end

Then(/^I receive success "([^"]*)"$/) do |message|
  expect(@login_page.success_message(message).text).to include(message)
end

Then(/^I click on Sign Out and return to Authentication page\.$/) do
  @login_page.click_on_signout
end

Then(/^I receive fail "([^"]*)"$/) do |message|
  expect(@login_page.fail_message(message).text).to include(message)
end